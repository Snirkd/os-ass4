#include "types.h"
#include "user.h"
#include "stat.h"
#include "fcntl.h"
#include "fs.h"

#define LESS_THAN_SIZE -1
#define EXACT_SIZE 0
#define GREATER_THAN_SIZE 1

#define TYPE_DIR 2
#define TYPE_FILE 3
#define TYPE_SYMLINK 4

struct findArgs {
  int followFlag;
  
  char fileName[100]; 
  int nameFlagHandled; // if flag -name is on

  int sizeIndicator; 
  int sizeConstraint;
  int sizeFlagHandled; // if flag -size is on

  int fileType;          
  int typeFlagHandled; // if flag -type is on

  char key[10]; 
  char val[30];
  char* valueDelimiter;
  int tagFlagHandled; // if flag -tag is on
};

void
printHelp(){
  printf(2,"Usage: find <path> <options> <tests>\n");
  printf(2,"Options:\n\t-follow: dereference all symbolic links\n");
  printf(2,"Tests:\n\t-name: all files named (no wildecards) fileName");
  printf(2,"\n\t-size (+/-)n: file is of size n (exactly), +n (more then n), -n (less then n)");
  printf(2,"\n\t-type <c>: File is of type <c>:");
  printf(2,"\n\t\td: directory");
  printf(2,"\n\t\tf: regular file");
  printf(2,"\n\t\ts: soft(symbolic) link");
  printf(2,"\n\t-tag <key>=<value>: File is tagged by a specified key-value pair.");
  printf(2,"\n\tIf value equals “?” then all files having the key key are matched,");
  printf(2,"\n\tregardless of the value. You may assume key and value do not contain spaces.\n");
  exit();
}

char*
fmtname(char *path)
{
  static char buf[DIRSIZ+1];
  char *p;

  // Find first character after last slash.
  for(p=path+strlen(path); p >= path && *p != '/'; p--)
    ;
  p++;

  // Return blank-padded name.
  if(strlen(p) >= DIRSIZ)
    return p;
  memmove(buf, p, strlen(p));
  memset(buf+strlen(p), ' ', DIRSIZ-strlen(p));
  return buf;
}

int 
testCheck(struct findArgs* args, int fd, struct stat stat, char* name){
  // Size and type from stat
  char tagBuff[100];
  char* questionMark = "?";

  if((args->nameFlagHandled == 1) && (strcmp(name, args->fileName) != 0)){
    // Does not pass the test for file name
    return 0;
  }

  if(args->sizeFlagHandled == 1){
    // Check that size qualifies:
    switch(args->sizeIndicator){
      case GREATER_THAN_SIZE:
        if(args->sizeConstraint > stat.size){
          return 0;
        }
        break;
      case EXACT_SIZE:
        if(args->sizeConstraint != stat.size){
          return 0;
        }
        break;
      case LESS_THAN_SIZE:
        if(args->sizeConstraint < stat.size){
          return 0;
        }
        break;
    }
  }

  if(args->typeFlagHandled == 1){
    switch(args->fileType){
      case TYPE_DIR:
        if(stat.type != T_DIR){
          return 0;
        }
        break;
      case TYPE_FILE:
         if(stat.type != T_FILE){
          return 0;
        }
        break;
      case TYPE_SYMLINK:
       if(stat.type != T_SYM){
          return 0;
        }
        break;
    }
  }

  if(args->tagFlagHandled == 1){
    if(gettag(fd, args->key, tagBuff) > 0){
      if(strcmp(args->val, questionMark) != 0){
        // no wildcard, compare actual val
        if(strcmp(args->val, tagBuff) != 0){
          return 0;
        }
      }
    }else{
      return 0;
    }
  }
  return 1; // Passed all tests!
}

int
find(char* path, char* name, struct findArgs* args){
  char buf[512];
  char *buf_offset;
  int fd;
  struct dirent dirent;
  struct stat stat;

  // printf(1, "PATH BEFORE: %s\n", path);

  
  if(args->followFlag == 1){
    readlink(path, buf, strlen(path)); // read the link into the buffer, we concat it later with the rest of the path.
  }
  
  // printf(1, "PATH: %s\n", path);
  // printf(1, "BUF: %s\n", buf);

  if((fd = open(path, O_RDONLY | O_DEREF)) < 0){
    printf(2, "No such file/directory...\n");
    return -1;
  }

  // Get file stat:
  if(fstat(fd, &stat) < 0){
    printf(2, "No stat found for file/directory...\n");
    close(fd);
    return -1;
  }

  // printf(1, "file name: %s, file type: %d, inode num: %d\n\n", path, stat.type, stat.ino);

  switch(stat.type){
    case T_FILE:
      //printf(1, "File, path: %s, inode: %d\n", path, stat.ino);
      if(testCheck(args, fd, stat, name) == 1){
        printf(1,"%s\n", path);
      }
      break;

    case T_DIR:
      //printf(1, "Directory, path: %s, inode: %d\n", path, stat.ino);
      strcpy(buf, path);

      // if the dir passes the test, print it out;`
      if(testCheck(args, fd, stat,fmtname(path)) == 1){
        printf(1, "%s\n", path);
      }
      buf_offset = buf + strlen(buf); // set offset to end of buf.

      if(strcmp(buf, "/") != 0){
        *buf_offset = '/';
        buf_offset++;
      }

      // Read folder entries:
      while(read(fd, &dirent, sizeof(dirent)) == sizeof(dirent)){ //  While we manage to read a new dirent
        if(dirent.inum == 0){ 
          continue;
        }

        memmove(buf_offset, dirent.name, strlen(dirent.name)); // concat the path
        buf_offset[strlen(dirent.name)] = 0; // concat null to the end of the path

        if(dirent.name[0] == '.'){
          continue; // dont go up
        }
        // call find on the new dirent:
        find(buf, dirent.name, args);
      }
      break;

      case T_SYM:
        if(testCheck(args, fd, stat, dirent.name) == 1){
          printf(1, "%s\n", path);
        }
        break;
  }
  close(fd);
  return 0;
}

int main(int argc, char *argv[]){

  char* pathName;
  char* formattedName;

  struct findArgs *args = malloc(sizeof(*args));

  int followFlag;
  
  char fileName[100]; // if flag -name is on
  int nameFlagHandled = 0;

  int sizeIndicator; // if flag -size is on
  int sizeConstraint;
  int sizeFlagHandled = 0;

  int fileType;          // if flag -type is on
  int typeFlagHandled = 0;

  char key[10]; // if flag -tag is on
  char val[30];
  char* valueDelimiter = "";
  int tagFlagHandled = 0;


  if(argc == 1){
    // No args, print usage for the function:
    printHelp();
  }

  // set pathName
  pathName = argv[1];

  // set formatted name (file name after last slash)
  formattedName = fmtname(pathName);

  if(strcmp(argv[2], "-follow") == 0){
    followFlag = 1;
    args->followFlag = 1;
  }else{
    followFlag = 0;
    args->followFlag = 0;
  }

  int i;
  for(i = 2 + followFlag; i < argc; i += 2){
    // Get arguments:

    if((strcmp(argv[i], "-name") == 0) && (nameFlagHandled == 0)) {  // Case -name:
      strcpy(fileName, argv[i+1]);
      nameFlagHandled = 1;
    }else if((strcmp(argv[i], "-size") == 0) && (sizeFlagHandled == 0)){ // Case -size:
      switch(argv[i+1][0]){
        case '+': 
          sizeIndicator = GREATER_THAN_SIZE;
          sizeConstraint = atoi(&argv[i+1][1]);
          break;
        case '-':
          sizeIndicator = LESS_THAN_SIZE;
          sizeConstraint = atoi(&argv[i+1][1]);
          break;
        default:
          // printf(1,"SIZE CONST\n");
          sizeIndicator = EXACT_SIZE;
          sizeConstraint = atoi(&argv[i+1][0]);
          // printf(1,"SZ CONST: %c\n", argv[i+1][1]);
          break;
      }
      sizeFlagHandled = 1;
    }else if((strcmp(argv[i], "-type") == 0) && (typeFlagHandled == 0)){ // Case -type
      switch(argv[i+1][0]){
        case 'd':
          fileType = TYPE_DIR;
          break;
        case 'f':
          fileType = TYPE_FILE;
          break;
        case 's':
          fileType = TYPE_SYMLINK;
          break;
        default:
          printHelp();

      }
      typeFlagHandled = 1;
    }else if((strcmp(argv[i], "-tag") == 0) && (tagFlagHandled == 0)){
      valueDelimiter = strchr(argv[i+1], '=');
      if(valueDelimiter == 0){
        printHelp();
      }
      valueDelimiter[0] = 0;
      strcpy(key, argv[i+1]);

      // in case of more than one "="
      char* valueRemainder;
      if((valueRemainder = strchr(valueDelimiter + 1, '=')) != 0){
        printHelp(); 
      }

      strcpy(val, valueDelimiter+1);

      valueDelimiter[0] = '=';
      tagFlagHandled = 1;
    }else{
      // Invalid Arguments
      printHelp();
    }

  }

  // printf(1, "[DEBUG] PathName: %s, Follow: %d\n", pathName, followFlag);

  if(nameFlagHandled == 1){
    memmove(args->fileName, fileName, strlen(fileName));
    // printf(1, "[DEBUG] -name: args->fileName:%s\n", args->fileName);
  }
  args->nameFlagHandled = nameFlagHandled;


  if(sizeFlagHandled == 1){
    args->sizeIndicator = sizeIndicator;
    args->sizeConstraint = sizeConstraint;
    // printf(1, "[DEBUG] -size: args->sizeIndicator: %d, args->sizeConstraint: %d\n", args->sizeIndicator, args->sizeConstraint);
  }  
  args->sizeFlagHandled = sizeFlagHandled;

  if(typeFlagHandled == 1){
    args->fileType = fileType;       
    // printf(1, "[DEBUG] -type: args->fileType: %d\n", args->fileType);
  }
  args->typeFlagHandled = typeFlagHandled;

  if(tagFlagHandled == 1){
    memmove(args->key, key, strlen(key));
    memmove(args->val, val, strlen(val));
    // printf(1, "[DEBUG] -tag: args->key: %s, args->value: %s\n", args->key, args->val);
  }
  args->tagFlagHandled = tagFlagHandled;

  find(pathName, formattedName, args);

  free(args);

  exit();
}
