#include "types.h"
#include "user.h"
#include "fcntl.h"

void test1(void) {
    printf(1, "Test 1: tag 3 tags, then get them, then untag them, then try to get them again, then tag again, and try to get again\n");

    int fd;
    if ((fd = open("tag_test", O_CREATE | O_RDWR)) < 0) {
        printf(2,"open failed\n");
        exit();
    }

    char* key1 = "k11111111111";
    char* key2 = "k2";
    char* key3 = "k3";
    char* value1 = "v1";
    char* value2 = "v2";
    char* value3 = "v3";

    char buffer[30];



    printf(1, "Tagging...\n");
    if (ftag(fd, key1, value1) == 0)
        printf(1, "[SUCCESS] ftag, key1 = %s, value1 = %s\n", key1, value1);
    else
        printf(2, "[FAIL   ] ftag, key1 = %s, value1 = %s\n", key1, value1);
    
    if (ftag(fd, key2, value2) == 0)
        printf(1, "[SUCCESS] ftag, key2 = %s, value2 = %s\n", key2, value2);
    else
        printf(2, "[FAIL   ] ftag, key2 = %s, value2 = %s\n", key2, value2);
    
    if (ftag(fd, key3, value3) == 0)
        printf(1, "[SUCCESS] ftag, key3 = %s, value3 = %s\n", key3, value3);
    else
        printf(2, "[FAIL   ] ftag, key3 = %s, value3 = %s\n", key3, value3);



    printf(1, "Getting...\n");
    if (gettag(fd, key1, buffer) > 0) {
        if (strcmp(buffer, value1) == 0) {
            printf(1, "[SUCCESS] gettag, key1 = %s, value1 = %s, buffer = %s\n", key1, value1, buffer);
        } else {
            printf(1, "[FAIL   ] gettag, key1 = %s, value1 = %s, buffer = %s\n", key1, value1, buffer);
        }
    } else {
        printf(2, "[FAIL   ] gettag, key = %s\n", key1);
    }
    memset(buffer, 0, strlen(buffer));

    if (gettag(fd, key2, buffer) > 0) {
        if (strcmp(buffer, value2) == 0) {
            printf(1, "[SUCCESS] gettag, key2 = %s, value2 = %s, buffer = %s\n", key2, value2, buffer);
        } else {
            printf(1, "[FAIL   ] gettag, key2 = %s, value2 = %s, buffer = %s\n", key2, value2, buffer);
        }
    } else {
        printf(2, "[FAIL   ] gettag, key = %s\n", key2);
    }
    memset(buffer, 0, strlen(buffer));

    if (gettag(fd, key3, buffer) > 0) {
        if (strcmp(buffer, value3) == 0) {
            printf(1, "[SUCCESS] gettag, key3 = %s, value3 = %s, buffer = %s\n", key3, value3, buffer);
        } else {
            printf(1, "[FAIL   ] gettag, key3 = %s, value3 = %s, buffer = %s\n", key3, value3, buffer);
        }
    } else {
        printf(2, "[FAIL   ] gettag, key = %s\n", key3);
    }
    memset(buffer, 0, strlen(buffer));



    printf(1, "Untagging...\n");
    if (funtag(fd, key1) == 0)
       printf(1, "[SUCCESS] funtag, key = %s\n", key1);
    else
       printf(2, "[FAIL   ] funtag, key = %s\n",key1);

    if (funtag(fd, key2) == 0)
       printf(1, "[SUCCESS] funtag, key = %s\n", key2);
    else
       printf(2, "[FAIL   ] funtag, key = %s\n",key2);

    if (funtag(fd, key3) == 0)
       printf(1, "[SUCCESS] funtag, key = %s\n", key3);
     else
       printf(2, "[FAIL   ] funtag, key = %s\n",key3);



    printf(1, "Getting again...\n");
    if (gettag(fd, key1, buffer) > 0) {
        if (strcmp(buffer, value1) == 0) {
            printf(1, "[SUCCESS] gettag, key1 = %s, value1 = %s, buffer = %s\n", key1, value1, buffer);
        } else {
            printf(1, "[FAIL   ] gettag, key1 = %s, value1 = %s, buffer = %s\n", key1, value1, buffer);
        }
    } else {
        printf(2, "[FAIL   ] gettag, key = %s\n", key1);
    }
    memset(buffer, 0, strlen(buffer));

    if (gettag(fd, key2, buffer) > 0) {
        if (strcmp(buffer, value2) == 0) {
            printf(1, "[SUCCESS] gettag, key2 = %s, value2 = %s, buffer = %s\n", key2, value2, buffer);
        } else {
            printf(1, "[FAIL   ] gettag, key2 = %s, value2 = %s, buffer = %s\n", key2, value2, buffer);
        }
    } else {
        printf(2, "[FAIL   ] gettag, key = %s\n", key2);
    }
    memset(buffer, 0, strlen(buffer));

    if (gettag(fd, key3, buffer) > 0) {
        if (strcmp(buffer, value3) == 0) {
            printf(1, "[SUCCESS] gettag, key3 = %s, value3 = %s, buffer = %s\n", key3, value3, buffer);
        } else {
            printf(1, "[FAIL   ] gettag, key3 = %s, value3 = %s, buffer = %s\n", key3, value3, buffer);
        }
    } else {
        printf(2, "[FAIL   ] gettag, key = %s\n", key3);
    }
    memset(buffer, 0, strlen(buffer));



    key1 = "k1";
    value1 = "t91";
    value2 = "t92";
    value3 = "t93";

    printf(1, "Tagging again...\n");
    if (ftag(fd, key1, value1) == 0)
        printf(1, "[SUCCESS] ftag, key1 = %s, value1 = %s\n", key1, value1);
    else
        printf(2, "[FAIL   ] ftag, key1 = %s, value1 = %s\n", key1, value1);
    
    if (ftag(fd, key2, value2) == 0)
        printf(1, "[SUCCESS] ftag, key2 = %s, value2 = %s\n", key2, value2);
    else
        printf(2, "[FAIL   ] ftag, key2 = %s, value2 = %s\n", key2, value2);
    
    if (ftag(fd, key3, value3) == 0)
        printf(1, "[SUCCESS] ftag, key3 = %s, value3 = %s\n", key3, value3);
    else
        printf(2, "[FAIL   ] ftag, key3 = %s, value3 = %s\n", key3, value3);



    printf(1, "Getting again...\n");
    if (gettag(fd, key1, buffer) > 0) {
        if (strcmp(buffer, value1) == 0) {
            printf(1, "[SUCCESS] gettag, key1 = %s, value1 = %s, buffer = %s\n", key1, value1, buffer);
        } else {
            printf(1, "[FAIL   ] gettag, key1 = %s, value1 = %s, buffer = %s\n", key1, value1, buffer);
        }
    } else {
        printf(2, "[FAIL   ] gettag, key = %s\n", key1);
    }
    memset(buffer, 0, strlen(buffer));

    if (gettag(fd, key2, buffer) > 0) {
        if (strcmp(buffer, value2) == 0) {
            printf(1, "[SUCCESS] gettag, key2 = %s, value2 = %s, buffer = %s\n", key2, value2, buffer);
        } else {
            printf(1, "[FAIL   ] gettag, key2 = %s, value2 = %s, buffer = %s\n", key2, value2, buffer);
        }
    } else {
        printf(2, "[FAIL   ] gettag, key = %s\n", key2);
    }
    memset(buffer, 0, strlen(buffer));

    if (gettag(fd, key3, buffer) > 0) {
        if (strcmp(buffer, value3) == 0) {
            printf(1, "[SUCCESS] gettag, key3 = %s, value3 = %s, buffer = %s\n", key3, value3, buffer);
        } else {
            printf(1, "[FAIL   ] gettag, key3 = %s, value3 = %s, buffer = %s\n", key3, value3, buffer);
        }
    } else {
        printf(2, "[FAIL   ] gettag, key = %s\n", key3);
    }
    memset(buffer, 0, strlen(buffer));

    

    printf(2, "End of test1\n\n");

    close(fd);
}


void test2(void) {
    printf(1, "Test 2: tag 3 tags, then get them, then change them, and try to get them again\n");

    int fd;
    if ((fd = open("tag_test", O_CREATE | O_RDWR)) < 0) {
        printf(2,"open failed\n");
        exit();
    }

    char* key1 = "k111";
    char* key2 = "k222";
    char* key3 = "k333";
    char* value1 = "v111";
    char* value2 = "v222";
    char* value3 = "v333";

    char buffer[30];



    printf(1, "Tagging...\n");
    if (ftag(fd, key1, value1) == 0)
        printf(1, "[SUCCESS] ftag, key1 = %s, value1 = %s\n", key1, value1);
    else
        printf(2, "[FAIL   ] ftag, key1 = %s, value1 = %s\n", key1, value1);
    
    if (ftag(fd, key2, value2) == 0)
        printf(1, "[SUCCESS] ftag, key2 = %s, value2 = %s\n", key2, value2);
    else
        printf(2, "[FAIL   ] ftag, key2 = %s, value2 = %s\n", key2, value2);
    
    if (ftag(fd, key3, value3) == 0)
        printf(1, "[SUCCESS] ftag, key3 = %s, value3 = %s\n", key3, value3);
    else
        printf(2, "[FAIL   ] ftag, key3 = %s, value3 = %s\n", key3, value3);



    printf(1, "Getting...\n");
    if (gettag(fd, key1, buffer) > 0) {
        if (strcmp(buffer, value1) == 0) {
            printf(1, "[SUCCESS] gettag, key1 = %s, value1 = %s, buffer = %s\n", key1, value1, buffer);
        } else {
            printf(1, "[FAIL   ] gettag, key1 = %s, value1 = %s, buffer = %s\n", key1, value1, buffer);
        }
    } else {
        printf(2, "[FAIL   ] gettag, key = %s\n", key1);
    }
    memset(buffer, 0, strlen(buffer));

    if (gettag(fd, key2, buffer) > 0) {
        if (strcmp(buffer, value2) == 0) {
            printf(1, "[SUCCESS] gettag, key2 = %s, value2 = %s, buffer = %s\n", key2, value2, buffer);
        } else {
            printf(1, "[FAIL   ] gettag, key2 = %s, value2 = %s, buffer = %s\n", key2, value2, buffer);
        }
    } else {
        printf(2, "[FAIL   ] gettag, key = %s\n", key2);
    }
    memset(buffer, 0, strlen(buffer));

    if (gettag(fd, key3, buffer) > 0) {
        if (strcmp(buffer, value3) == 0) {
            printf(1, "[SUCCESS] gettag, key3 = %s, value3 = %s, buffer = %s\n", key3, value3, buffer);
        } else {
            printf(1, "[FAIL   ] gettag, key3 = %s, value3 = %s, buffer = %s\n", key3, value3, buffer);
        }
    } else {
        printf(2, "[FAIL   ] gettag, key = %s\n", key3);
    }
    memset(buffer, 0, strlen(buffer));



    value1 = "t18";
    value2 = "t2888888";
    value3 = "t388";

    printf(1, "Changing...\n");
    if (ftag(fd, key1, value1) == 0)
        printf(1, "[SUCCESS] ftag, key1 = %s, value1 = %s\n", key1, value1);
    else
        printf(2, "[FAIL   ] ftag, key1 = %s, value1 = %s\n", key1, value1);
    
    if (ftag(fd, key2, value2) == 0)
        printf(1, "[SUCCESS] ftag, key2 = %s, value2 = %s\n", key2, value2);
    else
        printf(2, "[FAIL   ] ftag, key2 = %s, value2 = %s\n", key2, value2);
    
    if (ftag(fd, key3, value3) == 0)
        printf(1, "[SUCCESS] ftag, key3 = %s, value3 = %s\n", key3, value3);
    else
        printf(2, "[FAIL   ] ftag, key3 = %s, value3 = %s\n", key3, value3);



    printf(1, "Getting again...\n");
    if (gettag(fd, key1, buffer) > 0) {
        if (strcmp(buffer, value1) == 0) {
            printf(1, "[SUCCESS] gettag, key1 = %s, value1 = %s, buffer = %s\n", key1, value1, buffer);
        } else {
            printf(1, "[FAIL   ] gettag, key1 = %s, value1 = %s, buffer = %s\n", key1, value1, buffer);
        }
    } else {
        printf(2, "[FAIL   ] gettag, key = %s\n", key1);
    }
    memset(buffer, 0, strlen(buffer));

    if (gettag(fd, key2, buffer) > 0) {
        if (strcmp(buffer, value2) == 0) {
            printf(1, "[SUCCESS] gettag, key2 = %s, value2 = %s, buffer = %s\n", key2, value2, buffer);
        } else {
            printf(1, "[FAIL   ] gettag, key2 = %s, value2 = %s, buffer = %s\n", key2, value2, buffer);
        }
    } else {
        printf(2, "[FAIL   ] gettag, key = %s\n", key2);
    }
    memset(buffer, 0, strlen(buffer));

    if (gettag(fd, key3, buffer) > 0) {
        if (strcmp(buffer, value3) == 0) {
            printf(1, "[SUCCESS] gettag, key3 = %s, value3 = %s, buffer = %s\n", key3, value3, buffer);
        } else {
            printf(1, "[FAIL   ] gettag, key3 = %s, value3 = %s, buffer = %s\n", key3, value3, buffer);
        }
    } else {
        printf(2, "[FAIL   ] gettag, key = %s\n", key3);
    }
    memset(buffer, 0, strlen(buffer));

    printf(2, "End of test2\n\n");

    close(fd);
}


int main(int argc, char *argv[])
{
    test1();
    test2();

    exit();
}