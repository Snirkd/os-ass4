#include "types.h"
#include "stat.h"
#include "user.h"

int
main(int argc, char *argv[])
{
  if(((argc != 3) && (argc != 4)) || ((argc == 4) && (strcmp(argv[1], "-s") != 0))) {
    printf(2, "Hard link usage: ln old new\nSymbolic link usage: ln -s old new\n");
    exit();
  }
  
  if(argc == 3){
  	if(link(argv[1], argv[2]) < 0){
    	printf(2, "Hard link %s %s: failed\n", argv[1], argv[2]);
  	}
  	exit();
  }else{
  	if(symlink(argv[2], argv[3]) < 0){
  		printf(2, "Symbolic link %s %s: failed\n", argv[2], argv[3]);
  	}
  	exit();
  }
}
