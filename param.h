#define NPROC        64  // maximum number of processes
#define KSTACKSIZE 4096  // size of per-process kernel stack
#define NCPU          8  // maximum number of CPUs
#define NOFILE       16  // open files per process
#define NFILE       100  // open files per system
#define NINODE       50  // maximum number of active i-nodes
#define NDEV         10  // maximum major device number
#define ROOTDEV       1  // device number of file system root disk
#define MAXARG       32  // max exec arguments
#define MAXOPBLOCKS  10  // max # of blocks any FS op writes
#define LOGSIZE      (MAXOPBLOCKS*3)  // max data blocks in on-disk log
#define NBUF         (MAXOPBLOCKS*3)  // size of disk block cache
#define FSSIZE       32768  // size of file system in blocks SNIR 2/6/18 (2^15) new expanded max fs size
#define OLDFSSIZE  	 1000 // SNIR 2/6/18
#define MAX_DEREFERENCE 31 // SNIR 6/6/18
#define READLINK_DEREF 1 // SNIR 6/6/18
#define READLINK_NO_DEREF 0 // SNIR 6/6/18
